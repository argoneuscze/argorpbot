import random

from argorpbot.features.cards.card import Card
from argorpbot.features.cards.cardexception import CardException


class CardDeck:
    def __init__(self):
        self.cards = []
        self.shuffle()

    def shuffle(self):
        self.cards.clear()
        self.cards.extend(range(54))
        random.shuffle(self.cards)

    def draw(self):
        if not self.cards:
            raise CardException("Deck is empty, needs to be shuffled.")
        return Card(self.cards.pop())
