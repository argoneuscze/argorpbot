class Card:
    values = ("Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten",
              "Jack", "Queen", "King", "Ace", "Joker")
    suits = ("Clubs", "Diamonds", "Hearts", "Spades")

    def __init__(self, card_idx):
        self.card_idx = card_idx
        self.value = int(card_idx // 4)
        self.suit = card_idx % 4

    def __str__(self):
        name = self.values[self.value]
        if self.value < 13:
            name += " of {}".format(self.suits[self.suit])
        return name

    def get_index(self):
        return self.card_idx
