from abc import ABC, abstractmethod


class DieResult(ABC):
    @abstractmethod
    def get_value(self):
        pass

    def __str__(self):
        return "Not implemented."
