import random

from argorpbot.features.dice.types.die import Die
from argorpbot.features.dice.types.dieexception import DieException
from argorpbot.features.dice.types.regulardieresult import RegularDieResult


class RegularDie(Die):
    def __init__(self, sides):
        try:
            if sides < 1:
                raise DieException("Invalid number of sides.")
        except TypeError:
            raise
        self.sides = sides

    def roll(self):
        result = random.randint(1, self.sides)
        return RegularDieResult(result)
