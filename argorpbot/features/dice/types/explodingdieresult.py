from argorpbot.features.dice.types.dieresult import DieResult


class ExplodingDieResult(DieResult):
    def __init__(self, result, exploded):
        self.result = result
        self.exploded = exploded

    def get_value(self):
        return self.result

    def __str__(self):
        res = str(self.result)
        if self.exploded:
            res += '!'
        return res
