from argorpbot.features.dice.types.dieresult import DieResult


class RegularDieResult(DieResult):
    def __init__(self, result):
        self.result = result

    def get_value(self):
        return self.result

    def __str__(self):
        return str(self.result)
