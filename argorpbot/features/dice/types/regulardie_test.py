from argorpbot.features.dice.types.regulardie import RegularDie


def test_roll():
    d = RegularDie(6)
    d_res = d.roll()
    assert str(d_res) in ("1", "2", "3", "4", "5", "6")
