from abc import ABC, abstractmethod


class Die(ABC):
    @abstractmethod
    def roll(self):
        pass
