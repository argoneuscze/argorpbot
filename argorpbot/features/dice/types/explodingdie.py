import random

from argorpbot.features.dice.types.die import Die
from argorpbot.features.dice.types.dieexception import DieException
from argorpbot.features.dice.types.explodingdieresult import ExplodingDieResult


class ExplodingDie(Die):
    def __init__(self, sides):
        try:
            if sides < 2:
                raise DieException("Invalid number of sides.")
        except TypeError:
            raise
        self.sides = sides
        self.exploded = False

    def roll(self):
        result = random.randint(1, self.sides)
        total_result = result
        if result == self.sides:
            self.exploded = True
            while result == self.sides:
                result = random.randint(1, self.sides)
                total_result += result
        return ExplodingDieResult(total_result, self.exploded)
