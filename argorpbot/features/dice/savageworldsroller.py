from argorpbot.features.dice.exceptions import RollException

from argorpbot.features.dice.types.explodingdie import ExplodingDie


class SavageWorldsRoller:
    @staticmethod
    def trait_test(level, wildcard=True):
        try:
            if level < 2:
                raise RollException("Invalid skill level.")
        except TypeError:
            raise
        roll_result = ExplodingDie(level).roll()
        if wildcard:
            wild_result = ExplodingDie(6).roll()
            return roll_result, wild_result
        else:
            return roll_result
