import parsley

from argorpbot.features.dice.types.explodingdie import ExplodingDie
from argorpbot.features.dice.types.regulardie import RegularDie


class DiceParser:
    grammar = """
        S = NUM:amount 'd' TYPE:type OFFSET:offset -> roll_dice(amount, type, offset)
        NUM = <digit+>:number -> int(number)
        TYPE = ( NUM:maxvalue EX:explode -> ('d', maxvalue, explode) )
        EX = ( '!' -> 1
             | -> 0 )
        OFFSET = ( OP:operation NUM:value -> calc_offset(operation, value)
                 | -> 0 )
        OP = ( '+'
             | '-' )
        """

    def __init__(self):
        self.grammar = parsley.makeGrammar(self.grammar,
                                           {'roll_dice': DiceParser._roll_dice,
                                            'calc_offset': DiceParser._calc_offset})

    def parse(self, dice_string):
        return self.grammar(dice_string).S()

    @staticmethod
    def _roll_dice(amount, dice_type, offset):
        result = {'offset': offset}
        if dice_type[0] == 'd':
            dice_sides = dice_type[1]
            dice_explode = dice_type[2]
            dice_rolls = []
            for i in range(amount):
                if dice_explode:
                    d = ExplodingDie(dice_sides)
                else:
                    d = RegularDie(dice_sides)
                dice_rolls.append(d.roll())
            result['results'] = tuple(dice_rolls)
        return result

    @staticmethod
    def _calc_offset(op, val):
        if op == '+':
            return val
        return -val
