from ometa.runtime import ParseError

from argorpbot.features.cards.carddeck import CardDeck
from argorpbot.features.cards.cardexception import CardException
from argorpbot.features.dice.diceparser import DiceParser
from argorpbot.features.dice.exceptions import RollException
from argorpbot.features.dice.savageworldsroller import SavageWorldsRoller
from argorpbot.features.dice.types.dieexception import DieException


class CommandHandler:
    def __init__(self):
        self.dice_parser = DiceParser()
        self.sw_roller = SavageWorldsRoller()
        self.deck = CardDeck()

    def handle_command(self, msg):
        cmd, args = CommandHandler.parse_command(msg)
        try:
            return self.dispatch_table[cmd.lower()](self, *args)
        except KeyError:
            return None
        except (TypeError, ValueError):
            return "Invalid arguments."

    @staticmethod
    def parse_command(msg):
        spl = msg.strip().split()
        return spl[0], spl[1:]

    def cmd_roll(self, roll_string):
        msg = "Result:\n"
        try:
            roll = self.dice_parser.parse(roll_string)
            results = sorted(roll['results'], key=lambda x: x.get_value(), reverse=True)
            dice_str = " + ".join(str(x) for x in results)
            dice_sum = sum(x.get_value() for x in results)
            if not roll['offset']:
                msg += "({}) = **{}**".format(dice_str, dice_sum)
            else:
                msg += "({}) + [{}] = **{}**".format(dice_str, roll['offset'], dice_sum + int(roll['offset']))
        except ParseError:
            msg = 'Invalid dice expression.'
        except (RollException, DieException) as e:
            msg = e
        return msg

    def cmd_swtest(self, level_str):
        msg = "Savage Worlds Trait Test:\n"
        try:
            level = int(level_str)
        except ValueError:
            raise
        try:
            roll = self.sw_roller.trait_test(level)
        except RollException as r:
            return r
        msg += "Trait die: **{}**, Wild die: **{}**".format(str(roll[0]), str(roll[1]))
        return msg

    def cmd_shuffle(self):
        self.deck.shuffle()
        return "Deck shuffled."

    def cmd_draw(self):
        try:
            card = self.deck.draw()
        except CardException as e:
            return e
        return "You drew a **{}** ({}).".format(str(card), str(card.get_index()))

    dispatch_table = {
        'roll': cmd_roll,
        'swtest': cmd_swtest,
        'shuffle': cmd_shuffle,
        'draw': cmd_draw
    }
