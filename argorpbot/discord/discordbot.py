import discord

from argorpbot.config import config
from argorpbot.discord.commandhandler import CommandHandler

client = discord.Client()
ch = CommandHandler()


@client.event
async def on_ready():
    print("Successfully connected.")


@client.event
async def on_message(message):
    if message.author == client.user:
        return
    if message.content.startswith('!') and len(message.content) > 1:
        msg = ch.handle_command(message.content[1:])
        if msg:
            await client.send_message(message.channel, msg)


def start_discord_bot():
    client.run(config.BOT_TOKEN)
